import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  data = '';

  public youtubeResults;

  constructor() {
  }
  init() {
    this.getData("KEY_YOUTUBE_RESULTS").then((data: any) => {
      if (data) {
        this.youtubeResults = data;
      }
    });
  }
  public setData(KEY, data) {
    localStorage.setItem(KEY, JSON.stringify(data));
  }
  getData(KEY) {
    return new Promise(resolve => {
      this.data = JSON.parse(localStorage.getItem(KEY));
      resolve(this.data);
    })
  }
}
