import { SafePipe } from './../../pipes/safe.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DraggableModalComponent } from './draggable-modal.component';



@NgModule({
  declarations: [DraggableModalComponent,SafePipe],
  imports: [
    CommonModule
  ],exports:[DraggableModalComponent]
})
export class DraggableModalModule { }
